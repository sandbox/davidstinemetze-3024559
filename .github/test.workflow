workflow "Drupal.org Sync" {
  on = "push"
  resolves = ["Push to Drupal.org"]
}

action "vulnerable" {
  # needs = "8.x-1.x"
  uses = "./actions/vulnerable"
  secrets = ["MY_SUPER_SECRET_KEY"]
}
