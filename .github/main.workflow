workflow "Drupal.org Sync" {
  on = "push"
  resolves = ["Push to Drupal.org"]
}

# Filter for master branch
action "8.x-1.x" {
  uses = "actions/bin/filter@master"
  args = "branch 8.x-1.x"
}

action "Push to Drupal.org" {
  # needs = "8.x-1.x"
  uses = "./actions/push-to-drupal-org"
  secrets = ["DRUPAL_ORG_SSH_KEY"]
  env = {
    DRUPAL_ORG_GIT_REMOTE = "git@git.drupal.org:sandbox/davidstinemetze-3024559"
    DRUPAL_ORG_GIT_BRANCH = "8.x-1.x"
    DRUPAL_ORG_GIT_USER_NAME = "WidgetsBurritos"
    DRUPAL_ORG_GIT_USER_EMAIL = "davidstinemetze@gmail.com"
    DRUPAL_ORG_SSH_KNOWN_HOSTS = "git.drupal.org ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAuzswN6Bw/S/ifo7Z9wITwUoMbmjV+NlX4qvMHb/CC25lj2sXqGQDfq8dLwPQKNqkKBGHkyvJyTjjtyG35tC1KHa5KI1WFKfHbOMCkWLqQklCx2Com/W9NCQGK8sg/t/Y9ODE9RCwgYsQq9DnDWIfcLLp/UAIhXMNBVaBJhvENhsKgJaG4TsND/zn5za0Dt6JWLVgfKp61aaWDwATu+6ak+DzbZpfoZuawW9xcMbPjAsrGeuCUP2gsGRn8QVnd8Wx87/3Scf3QSNRnn0BLB/BQAKlVJlvKvfCqsKT4IAK1O2wnI+U4T2sKql8fYMHtFrcskbeG1yjkAVub+wvvWoJLQ=="
  }
}
