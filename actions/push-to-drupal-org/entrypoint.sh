#!/bin/sh -l

set -e

# Install openssh and git.
apk add --update git openssh-client

# Check for appropriate variables.
if [ ! -n "$DRUPAL_ORG_SSH_KEY" ]; then
  echo '**Missing drupal.org SSH key**'
  exit 1
fi
if [ ! -n "$DRUPAL_ORG_SSH_KNOWN_HOSTS" ]; then
  echo '**Missing drupal.org SSH known hosts**'
  exit 1
fi
if [ ! -n "$DRUPAL_ORG_GIT_REMOTE" ]; then
  echo '**Missing drupal.org git remote**'
  exit 1
fi
if [ ! -n "$DRUPAL_ORG_GIT_BRANCH" ]; then
  echo '**Missing drupal.org git remote**'
  exit 1
fi
if [ ! -n "$DRUPAL_ORG_GIT_USER_NAME" ]; then
  echo '**Missing git user name**'
  exit 1
fi
if [ ! -n "$DRUPAL_ORG_GIT_USER_EMAIL" ]; then
  echo '**Missing git user email**'
  exit 1
fi

# Generate SSH key and known hosts.
echo '**Generating SSH keys and known hosts**'
mkdir -m 0700 -p ~/.ssh
eval $(ssh-agent -s)
TEMP_KEY=$(mktemp ~/.ssh/drupal_key.XXXXXXXX)
cat > $TEMP_KEY <<EOF
$DRUPAL_ORG_SSH_KEY
EOF
chmod 0400 $TEMP_KEY
ssh-add $TEMP_KEY
KNOWN_HOSTS=~/.ssh/known_hosts
cat > $KNOWN_HOSTS <<EOF
$DRUPAL_ORG_SSH_KNOWN_HOSTS
EOF
chmod 0644 $KNOWN_HOSTS

# Configure git.
echo '**Configuring git**'
git remote add drupal $DRUPAL_ORG_GIT_REMOTE
git config user.name $DRUPAL_ORG_GIT_USER_NAME
git config user.email $DRUPAL_ORG_GIT_USER_EMAIL

echo '**Pushing to Drupal.org**'
# Push branch.
GIT_SSH_COMMAND="ssh -i $TEMP_KEY -o StrictHostKeyChecking=no" git push drupal $DRUPAL_ORG_GIT_BRANCH

# Clean up
echo '**Cleaning up**'
rm -f $TEMP_KEY $KNOWN_HOSTS
